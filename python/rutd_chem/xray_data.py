import numpy as np
import matplotlib.pyplot as plt
import h5py
import scipy.integrate as spinteg

def cross_section_VY95(E,Z,VYdat=None,N=None,S=None,L=None,Verbose=False):
    """
    Function to read the data tabulated in:

    Analytic fits for partial photoionization cross sections.
    D. A. Verner & D. G. Yakovlev, 1995, A&AS, 109, 125

    Byte-per-byte description of file: table1.dat
     -------------------------------------------------------------------------------
    Bytes Format  Units   Label        Eplanations
    -------------------------------------------------------------------------------
     1-  2  I2     ---     Z            Atomic number
     4-  5  I2     ---     N            Number of electrons
     7-  7  I1     ---     n            Principal quantum number of the shell 
     9-  9  I1     ---     l            Orbital quantum number of the subshell 
    11- 20  E10.4  eV      E_th         Subshell ionization threshold energy
    22- 31  E10.4  eV      E_0          Fit parameter
    33- 42  E10.4  Mb      \sigma_0     Fit parameter
    44- 53  E10.4  ---     y_a          Fit parameter
    55- 64  E10.4  ---     P            Fit parameter
    66- 75  E10.4  ---     y_w          Fit parameter
    -------------------------------------------------------------------------------

    Analytic formula to convert parameters into cross-sections:

     sigma_nl(E) = sigma_0 * F(E/E_0)   [Mb]

    F(y) = [ (y-1)^2 + y_w^2 ] * y^-Q * (1 + sqrt(y/y_a) )^-P

     where y = (E/E_0) and E has units of [eV], 

    Q = 5.5 + l - 0.5P 
  
     where l is the subshell orbital quantum number, l=0,1,2 (or s,p,d)
     
    Original code by M. Adamkovics
    """
    
    if not 'VYdat' not in locals() :
        file_VY95 = 'VY95_table1.dat'
        names_VY95 = ['Z','N','n','l','E_th','E_0','sigma','y_a','P','y_w']
        VYdat = np.genfromtxt(file_VY95, dtype=None, names=names_VY95).view(np.recarray)

    if N == None : N = Z

    subshells = np.intersect1d( np.where(VYdat.Z==Z)[0], np.where(VYdat.N==N)[0] )

    if S != None :
        subshells = subshells[np.where(VYdat.n[subshells]==S)[0]]

    if Verbose :
        print ("Input paramters: Z={:d}, N={:d}".format(Z,N))
        print ("subshell indices in data array: {}".format(subshells))
        
    cross_section = np.zeros((len(E),len(subshells)))
    
    j = 0
    for i in subshells :
        l     = VYdat.l[i]
        E_th  = VYdat.E_th[i]
        E_0   = VYdat.E_0[i]
        sigma = VYdat.sigma[i]
        y_a   = VYdat.y_a[i]
        P     = VYdat.P[i]
        y_w   = VYdat.y_w[i]

        y = E/E_0
        Q = 5.5 + l - 0.5*P
        F = ( (y-1)**2 + y_w**2 ) * (y**(-Q)) * (1 + np.sqrt(y/y_a) )**(-P)

        cross_section[:,j] = sigma * F * 1e-18   # [cm2] Mb -> cm2 included
        cross_section[E < E_th,j] = 0.0   
        
        j += 1

    return np.sum(cross_section, axis=1)

def cross_section_subshell(Z, E, VYdat=None):
    """
    Shell specific cross sections for a particular element, Z,
    over the energy range, E.
    """
    if Z >= 18 :
        sigma_M = cross_section_VY95(E, Z, S=3, VYdat=VYdat)
    else :
        sigma_M = np.zeros_like(E)
    
    if Z >= 3 :
        sigma_L = cross_section_VY95(E, Z, S=2, VYdat=VYdat)
    else :
        sigma_L = np.zeros_like(E)

    sigma_K = cross_section_VY95(E, Z, S=1, VYdat=VYdat)
    
    return (sigma_K, sigma_L, sigma_M)

def tabulate_cross_section_hdf5(file_VY95='VY95_table1.dat', 
                                h5_out='xray_cross_Verner95.hdf5',
                                names_VY95 = None, ephot=None, 
                                atomic_number=None):
    """
    Compute X-ray ionisation cross sections for elements specified in 
    atomic_number parameter on the ephot [eV] photon energy range, using the 
    fitting formula of Verner & Yakovlev (1995). The cross sections are saved 
    to the hdf5out HDF5 format file.
    
    Parameters
    ----------
    file_VY95 : str
            File name of Verner & Yakovlev (1995) coefficients.
            Default is "VY95_table1.dat"
    h5_out  : str
            Name of output HDF5 file
            Default is "xray_cross_Verner95.hdf5"
    names_VY95 : list
            List of strings, containing the name of columns in file_VY95. 
            Default is ['Z','N','n','l','E_th','E_0','sigma','y_a','P','y_w'].
    ephot   : double, numpy.ndarray
            Array of photon energies at which the cross section is computed.
            Default ranges between 1 eV and 100 keV, with 300 elements.
    atomic_number : dict
            Dictionary containing the element name and atomic number of species 
            for which the cross section is computed. E.g. {'H':1,'He':2,'C':6}.
            Default contains the following elements: H, He, C, N, O, Ne, Na, Mg,
            Al, Si, P, S, Cl, Ar, K, Ca, Ti, Cr, Mn, Fe, Co and Ni.
    """
    if ephot is None:
        ephot = 10**np.linspace(0,5,300)

    if atomic_number is None:
        atomic_number = {'H':1,'He':2,'C':6,'N':7,'O':8,'Ne':10,'Na':11,
                         'Mg':12,'Al':13,'Si':14,'P':15,'S':16,'Cl':17,
                         'Ar':18,'K':19,'Ca':20,'Ti':22,'Cr':24,'Mn':25,
                         'Fe':26,'Co':27,'Ni':28}

    if names_VY95 is None:
        names_VY95 = ['Z','N','n','l','E_th','E_0','sigma','y_a','P','y_w']

    VYdat = np.genfromtxt(file_VY95, dtype=None, names=names_VY95).view(np.recarray)

    db = h5py.File(h5_out,'w')

    for elem in atomic_number.iteritems():

        # Get cross sections
        sigma_K, sigma_L, sigma_M = cross_section_subshell(elem[1], ephot, 
                                                           VYdat=VYdat)

        # Save data to hdf5
        spec_group = db.create_group(elem[0])

        dset = spec_group.create_dataset('nr_data', dtype='i8', 
                                         data=len(ephot))
        dset = spec_group.create_dataset('anum', dtype='i8', data=elem[1])
        dset = spec_group.create_dataset('ephot', ephot.shape, dtype='f8', 
                                         data=ephot)
        dset = spec_group.create_dataset('sigma_K', sigma_K.shape, dtype='f8', 
                                         data=sigma_K)
        dset = spec_group.create_dataset('sigma_L', sigma_L.shape, dtype='f8', 
                                         data=sigma_L)
        dset = spec_group.create_dataset('sigma_M', sigma_M.shape, dtype='f8', 
                                         data=sigma_M)
    db.close()
    # Return
    return

def get_xray_cross_sec(species, h5db):
    """
    Extracts and returns X-ray cross section of species from h5db HDF5 file or 
    object.
    
    Parameters
    ----------
    species : str
            Species name, either chemical element or molecule. In the later case
            the cross section of component elements is summed.
    h5db    : hdf5 object or str
            Database containing cross section data. Either HDF5 object or name 
            of HDF5 file.

    Returns
    -------
    Dictionary containing photon energy (ephot), total (sigma_tot) and shell 
    cross sections (sigma_K, sigma_L, sigma_M), in ndarray format.
    """
    if type(h5db) is str:
        db = h5py.File(h5db,"r")
        close = True
    elif type(h5db) is h5py._hl.files.File:
        db = h5db
        close = False
    else:
        raise AssertionError('h5db is neither string nor h5py File object.')
    keys = db.keys()

    if species in keys:
        sigma_K = db[species]['sigma_K']
        sigma_L = db[species]['sigma_L']
        sigma_M = db[species]['sigma_M']
        ephot  = db[species]['ephot']
    else:
        tmp_spec = species
        sorted_elem = sorted(atomic_number.keys(), key=len,reverse=True)
        for elem in sorted_elem:
            n = tmp_spec.count(elem)
            if n >= 1:
                sigma_K =+ n * db[elem]['sigma_K'][:]
                sigma_L =+ n * db[elem]['sigma_L'][:]
                sigma_M =+ n * db[elem]['sigma_M'][:]
                ephot  = db[elem]['ephot']
                tmp_spec = tmp_spec.replace(elem,'')
                
    sigma_tot = np.array(sigma_K) + np.array(sigma_L) + np.array(sigma_M)
    
    if close:
        db.close()
    
    return {'ephot': ephot, 'sigma_tot':sigma_tot, 'sigma_K': sigma_K,
            'sigma_L': sigma_L, 'sigma_M': sigma_M}

def plot_xray_cross_sec(species, h5db):
    """
    Plots X-ray cross section of species. The cross section data is read from 
    h5db HDF5 file or object.
    
    Parameters
    ----------
    species : str
            Species name, either chemical element or molecule. In the later case
            the cross section of component elements is summed.
    h5db    : hdf5 object or str
            Database containing cross section data. Either HDF5 object or name 
            of HDF5 file.
    """
    cs = get_xray_cross_sec(species, h5db)

    # Plot cross sections
    plt.figure(figsize=(6,5))
    plt.loglog(cs['ephot'], cs['sigma_K'], label='K shell')
    plt.loglog(cs['ephot'], cs['sigma_L'], label='L shell')
    plt.loglog(cs['ephot'], cs['sigma_M'], label='M shell')
    plt.loglog(cs['ephot'], cs['sigma_tot'], label='Total cross section', 
               marker='o', markevery=3)
    plt.xlabel('photon energy [eV]')
    plt.ylabel('cross section [cm$^{2}$]')
    plt.title('{:s} x-ray cross section'.format(species))
    plt.legend()
    
    return

def compute_Lx_gorti(ephot, Lxtot=1.0e30):
    '''
    Computes X-ray spectrum using the piecewise formula from Gorti et al. 2009:
    
    Lx(E) \propto E           if 0.1 keV < E < 2 keV
    Lx(E) \propto E**(-1.75)  if 2 keV < E < 10 keV
    
    Parameters
    ----------
    ephot   : double, ndarray
            Array of photon energy in eV
    Lxtot   : double
            Total X-ray luminosity in erg/s
            Default is 1.0e30 erg/s
    '''
    Lx = np.zeros_like(ephot)

    low = np.where(ephot <= 2.0e3)
    high = np.where(ephot > 2.0e3)

    Lx[low] = ephot[low]
    Lx[high] = ephot[high]**-1.75

    Lx[low] = Lx[low] / np.sum(Lx[low])
    Lx[high] = Lx[high] * (Lx[low[0][-1]] / Lx[high[0][0]])

    Lx[np.where(ephot < 1.0e2)] = 0.0
    Lx[np.where(ephot > 1.0e4)] = 0.0

    Lx = Lx / np.sum(Lx) * Lxtot

    return Lx

def compute_Lx_aresu(ephot, kTx=1000., Lxtot=1.0e30):
    '''
    Computes parametric X-ray spectrum according Aresu et al. (2012).
    
    Parameters
    ----------
    ephot   : double, ndarray
            Array of photon energy in eV
    kTx     : double
            Hardness of radiation (radiation peak photon energy) in eV.
            Default is 1 keV.
    Lxtot   : double
            Total X-ray luminosity in erg/s
            Default is 1.0e30 erg/s
    '''
    kev = 1000.
    Lx = np.zeros_like(ephot)
    Lx = 1.0/ephot * np.exp(-ephot/kTx)
    
    Lx[np.where(ephot < 0.2*kev)] = 0.0
    Lx[np.where(ephot > 20.*kev)] = 0.0
    
    Lx = Lx / np.sum(Lx) * Lxtot
    
    return Lx

def compute_Fx_number(ephot, Lx, r=1.0, z=0.0, Nlos=0.0, sigma_tot=0.0):
    '''
    Computes unattenuated X-ray number flux at (r,z) location for a radiation 
    source of Lx luminosity.
    
    Parameters
    ----------
    ephot  :  float, np.ndarray
              Array of photon energy in eV.
    Lx     :  float, np.ndarray
              Unattenuated luminosity spectra of radiation source in erg/s.
    r      :  float
              Radial distance from the X-ray source in astronomical unit. 
              Default is 1.0.
    z      :  float
              Height above the disk midplane in cylindrical coordinates. Unit 
              is astronomical unit. Default value is 0.0.
    Nlos   :  float
              Line of sight hydrogen column density attenuating the radiation.
              Default is 0.0.
    sigma_tot : float
              X-ray cross section 
    '''
    au = 14960000000000.0 # cm
    erg2ev = 1./1.6021772E-12
    
    Fx = Lx / ( (r*au)**2 + (z*au)**2 )
    
    Fx_num = Fx / (ephot/erg2ev) * np.exp(-Nlos * sigma_tot)
    
    return Fx_num

def compute_xray_total_ionization(ephot, Fx, h5db, abuns=None):
    '''
    Compute the total X-ray ionisation rate, given material composition, 
    radiation spectrum and elemental ionisation cross section.
    
    Parameters
    ----------
    ephot  :  float, np.ndarray
              Array of photon energy in eV.
    Lx     :  float, np.ndarray
              Luminosity spectra of radiation source in erg/s.
    h5db   :  str or h5py.File
    abuns  :  dict
              Elemental composition of medium. The dictionary key defines the 
              species name, the value gives the relative abundance 
              (n_spec / n_Htot). If not given then elemental abundance from 
              Adamkovics et al. (2011) are assumed.
    Returns
    -------
    dictionary with the following keywords: abuns, sigma_tot, ephot, Fx, ZetaX.
    sigma_tot is the total X-ray cross section of the medium (float array-like).
    ZetaX is the total X_ray ionisation rate in the medium, in 1 / s unit.
    '''
    if abuns in None:
        # Abundances following Adamkovics et al. 2011
        abun = {'H'  :1.00,
                'He' :0.10,
                'C'  :1.4e-4,
                'N'  :6.0e-5,
                'O'  :3.5e-4,
                'Ne' :6.9e-5,
                'Na' :2.31e-7,
                'Mg' :1.0e-6,
                'K'  :8.57e-9,
                'Si' :1.68e-6,
                'S'  :1.4e-5,
                'Ar' :1.51e-6,
                'Fe' :1.75e-7}

    sigma_tot = np.zeros_like(ephot)

    for spec in abun.keys():
        cs_tmp = xray_data.get_xray_cross_sec(spec, 
                                              h5db='xray_cross_Verner95.hdf5')
        sigma_tot = sigma_tot + abun[spec] * np.interp(ephot, cs_tmp['ephot'], 
                                                       cs_tmp['sigma_tot'])
        prod = Fx*sigma_tot

        # Simple integration
        ZetaX = spinteg.trapz(prod,ephot)
    
    return {'abuns':abuns, 'sigma_tot':sigma_tot, 'ephot':ephot, 'Fx':Fx,
            'ZetaX':ZetaX}
