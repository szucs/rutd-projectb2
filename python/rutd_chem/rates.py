import numpy as np
import matplotlib.pyplot as plt
import h5py
import scipy.integrate as spinteg

from . import xray_data

ephot = 10.**np.arange(-1.5,2,0.01,dtype=np.double) * 1000.

Fx = compute_Fx_number(ephot)

​

def compute_rate(spec, shell, ephot, Fx, db=f):

    '''

    '''

    cs_tmp = xray_data.get_xray_cross_sec(spec, db=db)

    exec('sigma_u = cs_tmp[\'sigma_{}\']'.format(shell))

    sigma = np.interp(ephot, cs_tmp['ephot'], sigma_u)

    prod = Fx*sigma

    # Simple integration

    rate = spinteg.trapz(prod,ephot)

    return rate
​

def compute_xay_rate_ntw(reaction, ephot, Fx, h5db='xray_cross_Verner95.hdf5',
                         ZetaX0=1.7e-07, verbose=True):
    '''
    Update photochemical reaction rate coefficient class variable of reaction 
    class object.
    
    Parameters
    ----------
    reaction  : instance of reaction class
                Reaction to be updated. Code checks whether the reaction is 
                photochemical. If yes updates it using cross section and 
                spectra. If not then returns with -1.
    ephot     : array like, float
                Array of photon energy expressed in electron volts.
    Fx        : array like, float
                Photon flux at energies defined by ephot. The unit is photons 
                per second. 
    h5db      : str or h5py.File
                Name or handler of the HDF5 file containing the photochemical 
                cross sections. Default is 'xray_cross_Verner95.hdf5'.
    ZetaX0    : float
                X-ray ionisation rate normalisation factor. In the chemical code 
                the ak(X-ray) = alpha * ZetaX = ak(spectrum) * ZetaX / Zeta0.
    '''
    if reaction.r2 == 'XRAYS':

        sigma = np.zeros_like(ephot)

        # Electron shell configuration
        xray_data_str = reaction.comment.replace('zeta','')

        sets = xray_data_str.strip().split('+')

        for s in sets:
            s = s.replace('[','')
            spec = s.split(']')[0]
            shell = s.split(']')[1]
            cs_tmp = xray_data.get_xray_cross_sec(spec, h5db=db)
            exec('sigma_u = cs_tmp[\'sigma_{}\']'.format(shell))
            sigma = sigma + np.interp(ephot, cs_tmp['ephot'], sigma_u)

        prod = Fx*sigma

        # Simple integration
        rate = spinteg.trapz(prod,ephot)


        reaction.alpha = rate / ZetaX0

        if verbose:
            print '{:40s}: rate = {:.2E}'.format(repr(reaction), reaction.alpha)

        return 0

    else:

        return -1

