from __future__ import print_function

import numpy as np
import os
import operator

from collections import OrderedDict

class species:
    '''
    Species class containing name, atomic mass, composition, unique ID and 
    binding energy of chemical species.
    '''
    name   = ''
    chemid = None
    grain  = None
    ebind  = np.nan
    anum   = np.nan
    amas   = np.nan
    charge = np.nan
    elem   = {}            # elements and their number in species
    hash   = np.long(0)            # hash for comparison
    
    def __init__(self, name='', chemid=None, char_exchange=None, 
                 word_exchange=None):
        '''
        Initialise species object.
        
        :param name: species name string
        :param chemid: unique chemical identifier of species if any
        :param exchange: translation between read in and internal species notation
        :type exchange: Dict or OrderedDict
        '''
        self.name = name
        self.conv_notation(char_exchange=char_exchange, 
                           word_exchange=word_exchange)
        
        self.chemid = chemid
        self.hash   = hash(self.name)
        
        self.count_atoms()
        self.count_charge()

        self.grain = False   # is grain surface species?
        self.ebind = 0.0

    def __str__(self):
        return str(self.name)
    
    def __repr__(self):
        return "Species({})".format(self.name)
    
    def __eq__(self, other):
        if type(other) == str:
            compare =  (self.name == other)
        elif isinstance(other,species):
            compare =  (self.hash == other.hash)
        else:
            raise ValueError('ERROR: object type does not match!')

        return compare
    
    def __nonzero__(self):
        if self.name == '':
            return False
        else:
            return True
    
    def __hash__(self):
        return self.hash
        
    def conv_notation(self, char_exchange=None, word_exchange=None):
        '''
        Convert between external and internal species name notation.
        
        :param char_exchanges: translation list between notations
        :type param: Dict or OrderedDict
        :param word_exchanges: translation list between notations
        :type param: Dict or OrderedDict
        '''
        if char_exchange == None:
            char_exchange = {'X'     : 'g',
                             'J'     : 'g'}
        if word_exchange == None:
            word_exchange = {'Photon': 'PHOTON',
                             'GRAIN' : 'G0',
                             'GRAIN0': 'G0',
                             'GRAIN-': 'G-',
                             'GRAIN+': 'G+',
                             'e-'    : 'ELECTR',
                             'XRAYS' : 'XRAYS',
                             'XPHOT' : 'XPHOT'}

        # Stop if zero species
        if self.name in ['',None]:
            return
        
        for wex in word_exchange:
            if wex == self.name:
                self.name = word_exchange[wex]
                return
                
        for cex in char_exchange:
            if cex in self.name:
                self.name = self.name.replace(cex,char_exchange[cex],1)
                return

    def count_charge(self):
        '''
        Determines the charge number and sign from the species name.
        '''
        positive = self.name.count("+")
        negative = self.name.count("-") * -1
        
        self.charge = positive + negative
        
        return
        
    def count_atoms(self):
        '''
        Determine species elemental composition and properties.
        '''
        # Initialization of atom's names:
        elements = OrderedDict([
               # Some networks use all capital species...
               (    'HE', 4.00260E+00),
               (    'NE', 2.01830E+01),
               (    'NA', 2.29898E+01),
               (    'MG', 2.43050E+01),
               (    'AL', 2.69815E+01),
               (    'SI', 2.80855E+01),
               (    'CL', 3.54527E+01),
               (    'AR', 3.99480E+01),
               (    'CA', 4.00800E+01),
               (    'TI', 4.79000E+01),
               (    'CR', 5.19960E+01),
               (    'MN', 5.49380E+01),
               (    'FE', 5.58470E+01),               
               # ... others do not
               ('ELECTR', 5.44624E-07),
               ('PHOTON', 0.00000E+00),
               ('CRPHOT', 0.00000E+00),
               (   'CRP', 0.00000E+00),
               (    'He', 4.00260E+00),
               (    'Ne', 2.01830E+01),
               (    'Na', 2.29898E+01),
               (    'Mg', 2.43050E+01),
               (    'Al', 2.69815E+01),
               (    'Si', 2.80855E+01),
               (    'Cl', 3.54527E+01),
               (    'Ar', 3.99480E+01),
               (    'Ca', 4.00800E+01),
               (    'Ti', 4.79000E+01),
               (    'Cr', 5.19960E+01),
               (    'Mn', 5.49380E+01),
               (    'Fe', 5.58470E+01),
               (     'H', 1.00000E+00),
               (     'C', 1.20111E+01),
               (     'N', 1.40067E+01),
               (     'O', 1.59994E+01),
               (     'F', 1.89984E+01),
               (     'P', 3.09738E+01),
               (     'S', 3.20660E+01)])

        # Remove prefixes
        prefx  = ['g','o','p','m','l-','c-','J']
        postfx = ['*']
        
        # Stop if zero species
        if self.name in ['',None]:
            return
        
        # 
        name_tmp = self.name
        amas_tmp = 0.0
        anum_tmp = 0.0
        elem_tmp = {}
        
        # remove prefixes
        for pfx in prefx:
            if pfx in name_tmp[0:len(pfx)]:
                name_tmp = name_tmp[len(pfx):]
                
        for pfx in postfx:
            if pfx in name_tmp[-len(pfx):]:
                name_tmp = name_tmp[0:-len(pfx)]
        
        #
        for elm in elements:
                
            while elm in name_tmp:
                
                sindex = name_tmp.find(elm)
                lenelm = len(elm)
                
                if sindex >= 0:

                    # check if multiple atoms:
                    pos_s = sindex+lenelm
                    pos_f = min([pos_s+2,len(name_tmp)])
                
                    #TODO: make multiplier counting universal
                    if name_tmp[pos_s:pos_f].isdigit():
                        multi = float(name_tmp[pos_s:pos_f])
                        # remove multiplier from string
                        name_tmp = name_tmp[:pos_s] + name_tmp[pos_f:]
                    elif name_tmp[pos_s:pos_f-1].isdigit():
                        multi = float(name_tmp[pos_s:pos_f-1])
                        # remove multiplier from string
                        name_tmp = name_tmp[:pos_s] + name_tmp[pos_f-1:]
                    else:
                        multi = 1.0
                    
                    if elm in elem_tmp:
                        elem_tmp[elm] += multi
                    else:
                        elem_tmp.update({elm: multi})

                    anum_tmp += multi
                    amas_tmp += elements[elm] * multi
                    
                    # remove element from string
                    name_tmp = name_tmp[:sindex] + name_tmp[pos_s:]
                else:
                    pass
                
        # Save & return
        self.amas, self.anum, self.elem = amas_tmp, anum_tmp, elem_tmp
        
        return
    
    def select(self, anum=1000, charge=None, elem='', relate='>=', crit='any'):
        '''
        Determines whether to select species based on atom number and/or 
        contained element criteria. (True if property >= than criteria)
        
        :param anum: atom number criteria for selection
        :type amax: int
        :param elem: element criteria for selection
        :type elem: list or str or dict
        :param relate: comparison operator (e.g. '>', '==', etc.)
        :type relate: str
        '''
        ops = {'>' : operator.gt,
               '<' : operator.lt,
               '>=': operator.ge,
               '<=': operator.le,
               '==': operator.eq,
               '!=': operator.ne}
        opc = ops[relate]
        
        comps = {'any': operator.or_,
                 'all': operator.and_}
        crc = comps[crit]
        
        chk_anum = False
        if opc(self.anum, anum):
            chk_anum = True

        chk_elem = False
        elem_dict = ( type(elem) == dict )
        chk_elem_arr = []
        for re in elem:
            if re in self.elem:
                if elem_dict:
                    if opc(self.elem[re], elem[re]):
                        chk_elem_arr.append(True)
                    else:
                        chk_elem_arr.append(False)
                        
                else:
                    chk_elem = True
        if elem_dict:
            chk_elem_arr = np.array(chk_elem_arr)
            if crit == 'any':
                chk_elem = chk_elem_arr.any()
            else:
                chk_elem = chk_elem_arr.all()
        
        if charge is None:
            chk_charge = True
        elif opc(self.charge, charge):
            chk_charge = True
        else:
            chk_charge = False
        
        comp_chrg = crc(chk_elem,chk_charge)
        if crc(chk_anum,comp_chrg):
            return True
        else:
            return False


class reaction():
    '''
    Reaction class containing reactants and products, reaction rate coefficient 
    parametrisation and reaction meta data (type, comment, multiplicity, etc.).
    
    Reaction class provides interface for comparing, updating and evaluating 
    (based on some criteria) reactions.
    '''
    index = 0
    
    r1, r2     = species(), species()
    p1, p2, p3 = species(), species(), species()
    p4, p5     = species(), species()
    
    alpha, beta, gamma = np.nan, np.nan, np.nan
    
    Tmin = np.nan
    Tmax = np.nan
    
    formul = -1
    multiform = -1
    nform = -1
    
    # Rate coefficients (time dependent) may be stored
    ak   = np.nan
    nt   = 0
    time = np.nan
    
    comment = ''
    
    spec = []
    elem = []
    
    hash = np.long(0)
    
    def __init__(self, index, r1, r2, p1, p2, p3, p4, p5, alpha, beta, gamma,
                 Tmin, Tmax, rtype, formul, multiform, nform=1, comment='', 
                 char_exchange=None, word_exchange=None, **kwargs):
        '''
        char_exchange and word_exchange keywords control notation conversion.
        see species.conv_notation() method.
        '''
        self.index = np.int(index)
    
        self.r1 = species(r1.strip(), char_exchange=char_exchange, 
                          word_exchange=word_exchange)
        self.r2 = species(r2.strip(), char_exchange=char_exchange, 
                          word_exchange=word_exchange)
        self.p1 = species(p1.strip(), char_exchange=char_exchange, 
                          word_exchange=word_exchange)
        self.p2 = species(p2.strip(), char_exchange=char_exchange, 
                          word_exchange=word_exchange)
        self.p3 = species(p3.strip(), char_exchange=char_exchange, 
                          word_exchange=word_exchange)
        self.p4 = species(p4.strip(), char_exchange=char_exchange, 
                          word_exchange=word_exchange)
        self.p5 = species(p5.strip(), char_exchange=char_exchange, 
                          word_exchange=word_exchange)
        
        self.alpha = np.double(alpha) # np.double( kwargs.get('alpha',None) )
        self.beta = np.double(beta) # np.double( kwargs.get('beta',None) )
        self.gamma = np.double(gamma) # np.double( kwargs.get('gamma',None) )
        self.comment = comment
        self.Tmin = np.double(Tmin) #np.double( kwargs.get('Tmin',None) )
        self.Tmax = np.double(Tmax) #np.double( kwargs.get('Tmax',None) )
        self.rtype  = np.int(rtype) #np.int( kwargs.get('rtype',None) )
        self.formul = np.int(formul) #np.int( kwargs.get('formul',None) )
        self.multiform = np.int(multiform) #np.int( kwargs.get('multiform',None) )
        self.nform = np.int(nform)
        
        self.count_species()
        
        self.compute_hash()

    def __hash__(self):
        return self.hash
    
    def __repr__(self):
        rea_repr = '{} + {} -> {}'.format(self.r1, self.r2, self.p1)
        
        if bool(self.p2):
            rea_repr += ' + {}'.format(self.p2)
        if bool(self.p3):
            rea_repr += ' + {}'.format(self.p3)
        if bool(self.p4):
            rea_repr += ' + {}'.format(self.p4)
        if bool(self.p5):
            rea_repr += ' + {}'.format(self.p5)
        
        return rea_repr
    
    def __str__(self):
        rea_string = '%6u %-12s%-12s%-12s%-12s%-12s%-12s%-12s%-12s%9.2E%9.2E%9.2E %6u %6u %9.2E %9.2E %6u %6u\n' % ( self.index, self.r1, self.r2,' ', self.p1, self.p2, 
                    self.p3, self.p4, self.p5, self.alpha, self.beta, 
                    self.gamma, self.rtype, self.formul, 
                    self.Tmin, self.Tmax+1., self.multiform, self.nform)
        
        return rea_string
    
    def __eq__(self, other):
        if isinstance(other,reaction):
            compare =  (self.hash == other.hash)
        else:
            raise ValueError('ERROR: object type does not match!')

        return compare

    def __nonzero__(self):
        if bool(self.r1) and bool(self.p1):
            return True
        else:
            return False
    
    def select(self, anum=1000, charge=None, elem='', relate='>=', crit='any'):
        '''
        Determines whether to reject reaction based on atom number and/or 
        contained element criteria.
        
        :param anum: atom number criteria for selection
        :type amax: int
        :param charge: charge number criteria for selection
        :type charge: int
        :param elem: element criteria for selection
        :type elem: list or str or dict
        :param relate: comparison operator (e.g. '>', '==', etc.)
        :type relate: str
        :param crit: criteria strength: if crit='any' then return true if criteria
                     is true for any reaction species; if crit='all' then 
                     return true if criteria true for all reaction species.
        :type crit: str
        '''
        rs = []

        for s in self.spec:
            rs.append(s.select(anum=anum, charge=charge, elem=elem, 
                               relate=relate, crit=crit))
        rs = np.array(rs)
        
        if crit == 'any':
            return rs.any()
        else:
            return rs.all()
        
    def count_species(self):
        '''
        Search species and elements in reactants and products.
        '''
        spec_set = set([self.r1,self.r2,self.p1,self.p2,
                        self.p3,self.p4,self.p5])
        spec_set.discard( species() )
        
        self.spec = list(spec_set)
        
        elem_set = set(list(self.r1.elem.keys())+list(self.r2.elem.keys())+
                       list(self.p1.elem.keys())+list(self.p2.elem.keys())+
                       list(self.p3.elem.keys())+list(self.p4.elem.keys())+
                       list(self.p5.elem.keys()))
        self.elem = list(elem_set)
        
        return
    
    def compute_hash(self):
        '''
        Compute reaction hash.
        '''
        self.hash = hash((self.r1,self.r2,self.p1,self.p2,self.p3,self.Tmin,self.Tmax))
        
        return
    
class network():
    '''
    Network class is a collection of reaction class objects.
    '''    
    def __init__(self, comment='chemical network'):
        self.reactions = []
        self.spec   = []
    
        self.nre = 0
        self.nsp = 0
        self.comment = comment
        
    def __str__(self):
        return self.comment
    
    def __repr__(self):
        return "Network with {} reactions between {} species".format(self.nre, 
                                                                     self.nsp)
    
    def __add__(self, other):
        if isinstance(other, network):
            new_network = network('merged network: {} & {}'.format(self.comment,
                                                                   other.comment))
            new_network.add_reaction(self.reactions+other.reactions)
        else:
            raise TypeError('not network class object')
        
        return new_network
    
    def __getitem__(self, i):
        if type(i) == int:
            data = self.reactions[i]
        elif type(i) in [list, np.ndarray]:
            data = []
            
            if type(i[0]) in [bool, np.bool_]:
                for j in range(len(i)):
                    if i[j]:
                        data.append(self.reactions[j])
                    
            else:
                for j in i:
                    data.append(self.reactions[j])
        else:
            raise TypeError('only integer scalar, list and np.ndarray can be \
                converted to scalar index')
        
        return data
    
    def index(self, react):
        '''
        Returns the index of matching reaction in the network.
        '''
        return self.reactions.index(react)
    
    def add_reaction(self, react, update=False):
        '''
        Add reaction or a list of reactions to network.
        
        :param react: single reaction, list or network of reactions to be added
        :type react: reaction class object, list or network class object
        '''
        if isinstance(react,reaction):
            react = [react]
        elif isinstance(react,network):
            react = react.reactions

        for rea in react:
            self.reactions.append(rea)
            self.nre = len(self.reactions)
        
        self.update_spec()
        
        return
    
    def add_reaction_check(self, react, update=False):
        '''
        Add reaction or a list of reactions to network. Same as add_reaction()
        but checks if reaction is already in network. Optionally updates 
        existing reaction meta data with new.
        
        :param react: single reaction, list or network of reactions to be added
        :type react: reaction class object, list or network class object
        :param update: if reaction is already in network then update with the 
                       newly added reaction parameters (default False)
        :type react: Logical
        '''
        if isinstance(react,reaction):
            react = [react]
        elif isinstance(react,network):
            react = react.reactions

        for rea in react:
            # reaction already in network?
            if rea in self.reactions:
                print( "INFO: reaction {} already in network. Update: {}".format(
                    repr(rea),update) )
                # update if requested
                if update:
                    self.upd_reaction(rea)
            else:
                self.reactions.append(rea)
                self.nre = len(self.reactions)
            
                spec_set = set(self.spec+rea.spec)
                self.spec = list(spec_set)
                self.nsp = len(self.spec)
                
        return
    
    def del_reaction(self, react):
        '''
        Delete reaction or reactions from network.
        
        :param react: single reaction, list or network of reactions to be deleted
        :type react: reaction class object, list or network class object
        '''
        if isinstance(react,reaction):
            react = [react]
        elif isinstance(react,network):
            react = react.reactions
        
        isdeleted = False
        
        for rea in react:
            try:
                ind = self.reactions.index(rea)
                self.reactions = self.reactions[:ind] + self.reactions[ind+1:]
                self.nre = len(self.reactions)
                isdeleted = True
            except:
                pass
        
        if isdeleted:
            self.update_spec()
            
        return
    
    def upd_reaction(self, react):
        '''
        Update reaction properties.
        
        :param react: single reaction, list or network of reactions to be updated
        :type react: reaction class object, list or network class object
        '''
        if isinstance(react,reaction):
            react = [react]
        elif isinstance(react,network):
            react = react.reactions
        
        for rea in react:
            try:
                ind = self.reactions.index(rea)
                self.reactions[ind].alpha = rea.alpha
                self.reactions[ind].beta  = rea.beta
                self.reactions[ind].gamma = rea.gamma
                self.reactions[ind].Tmin = rea.Tmin
                self.reactions[ind].Tmax = rea.Tmax
                self.reactions[ind].rtype = rea.rtype
                self.reactions[ind].formul = rea.formul
                self.reactions[ind].multiform = rea.multiform
                self.reactions[ind].nform = rea.nform
            except:
                pass
            
        return

    def update_spec(self):
        '''
        Check consistency between species list and reactions. 
        
        When reactions are added or removed then it is useful to update the 
        species list.
        '''
        self.spec = []
        
        for rea in self.reactions:
            self.spec = self.spec+rea.spec
            
        # Organise species list, discard non-species string
        discards = ['CRP','CRPHOT','DESORB','FREEZE','PHOTON','XRAYS','CR','XPHOT']
        spec_set = set(self.spec)
        for dc in discards:
            spec_set.discard(dc)
        self.spec = list(spec_set)
        self.nsp = len(self.spec)
        
        return

    def find(self, crit):
        '''
        Returns index of reaction(s) matching the criteria.
        
        Examples: network.find('r1 == CO')
        '''
        # parse criteria
        # TODO: allow list crit parameter.
        #if type(crit) == str:
            #nr_crit  = 1
        #else:
            #nr_crit = len(crit)
        
        findex = np.zeros(self.nre, dtype=np.bool)
        
        crit_list = crit.split()
        var = crit_list[0]
        ops = {'>' : operator.gt,
               '<' : operator.lt,
               '>=': operator.ge,
               '<=': operator.le,
               '==': operator.eq,
               '!=': operator.ne}
        opc = ops[crit_list[1]]
        spec = crit_list[2]
        
        # Determine which component to test
        for i in range(self.nre):
            exec('rp_crit = self.reactions[i].{}'.format(var))
            
            if opc(rp_crit.name, spec):
                findex[i] = True
                
        return findex
        

    def find_duplicates(self):
        '''
        Search network for duplicate reactions.
        '''
        hashes = []
        nouniq = []
        
        for i in range(self.nre):
            chsh = self.reactions[i].hash
            if chsh in hashes:
                print( "WARN: duplicate reaction found: {}".format(
                    repr(self.reactions[i])) )
                nouniq.append(i)
            else:
                hashes.append(chsh)
                
        return nouniq
            
    def select(self, anum=1000, charge=None, elem='', relate='>=', crit='any'):
        '''
        Remove reactions from network based on criteria on number of atoms in 
        species and/or element in species.
        
        :param anum: atom number criteria for selection
        :type amax: int
        :param charge: charge number criteria for selection
        :type charge: int
        :param elem: element criteria for selection
        :type elem: list or str or dict
        :param relate: comparison operator (e.g. '>', '==', etc.)
        :type relate: str
        :param crit: criteria strength: if crit='any' then return true if criteria
                     is true for any reaction species; if crit='all' then 
                     return true if criteria true for all reaction species.
        :type crit: str
        '''
        selections = np.ndarray(self.nre, dtype=bool)
        
        count = 0
        for reac in self.reactions:
            selections[count] = reac.select(anum=anum, charge=charge, elem=elem, 
                                            relate=relate, crit=crit)
            count += 1

        return selections
            
    def write2alchemic(self, networkfile='rreactions_kida.dat',
                       specfile='rspecies_kida.dat'):
        '''
        Write network in the ALCHEMIC input format.

        :param networkfile: network file (default rreactions_kida.dat)
        :type networkfile: str
        :param specfile: species file (default rspecies_kida.dat)
        :type specfile: str
        '''
        
        if os.path.isfile(networkfile):
           print( 'File', networkfile, 'already exists!' )
           what2do = input('Do you want to overwrite? (y/n)')
           if what2do == 'n' or what2do == 'no':
              print( 'No file was written!' )
              return
          
        outfile = open(networkfile, 'w')
        # Write the header
        outfile.write( ('%6u ' + self.comment + '\n') % self.nre )
        for reac in self.reactions:
            outfile.write(str(reac))
        outfile.close()
        print( 'File {} is written!'.format(networkfile) )
        
        # Write the species file
        if os.path.isfile(specfile):
           print( 'File {} already exists!'.format(specfile) )
           what2do = input('Do you want to overwrite? (y/n)')
           if what2do == 'n' or what2do == 'no':
              print( 'No file was written!' )
              return

        outfile = open(specfile, 'w')
        outfile.write( ('%6u ' + self.comment + '\n') % self.nsp )
        for s in self.spec:
            outfile.write( '%-12s\n' % str(s) )
        outfile.close()
        print( 'File', specfile, 'is written!' )
        
        return
        
    #def fromfile(self, filename, form):
    
def read_kida(input_file, comment='kida network', skip_char=['!','#','\n']):
    '''
    Reads network file to network class
    '''
    f = open(input_file, 'r')
    linecounter = 0
    
    # Define notation conversion
    word_exchange = {'Photon': 'PHOTON',
                     'GRAIN0': 'G0',
                     'GRAIN-': 'G-',
                     'GRAIN+': 'G+',
                     'e-'    : 'ELECTR',
                     'XRAYS' : 'XRAYS',
                     'XPHOT' : 'XPHOT',
                     'CR'    : 'CRP',
                     'CRP'   : 'CRPHOT'}
    
    # initialise network
    network_ = network(comment=comment)
    
    reac_list = []
    
    for line in f:
        if (line[0] not in skip_char):
            # TODO: replace this with regular expressions!
            r1 = line[0:10] 
            r2 = line[10:20]
            p1 = line[33:44]
            p2 = line[44:55]
            p3 = line[55:66]
            p4 = line[66:77]
            p5 = ''
            
            if 'GRAIN' in r1:
                r1 = line[10:20]
                r2 = line[0:10]

            alpha = line[90:100]
            beta = line[101:111]
            gamma = line[112:122]
            # Temperature dependences
            Tmin = line[150:155]
            Tmax = line[157:162]

            index  = line[166:171]
            rtype  = line[146:148]
            formul = line[163:165]
            multiform = line[172:173] # are there duplicate, higher
                                      # version of this reaction?
            
            reac = reaction(index, r1, r2, p1, p2, p3, p4, p5, 
                                  alpha, beta, gamma, Tmin, Tmax, 
                                  rtype, formul, multiform, nform=1,
                                  word_exchange=word_exchange)
            reac_list.append(reac)
    
    f.close()
    print( 'Reading file {} is done!\n'.format(input_file) )
    
    # Add reaction list to network
    network_.add_reaction(reac_list)
    #network_.add_reaction_check(reac_list)  # same as above but slower
    
    # Return the network
    return network_

def read_mpchem(input_file, comment='mpchem network', skip_char=['!','#','\n']):
    '''
    Reads network file to network class
    '''
    f = open(input_file, 'r')
    linecounter = 0
    
    # initialise network
    network_ = network(comment=comment)
    
    reac_list = []
    
    fc = 0
    for line in f:
        if fc == 0:
            fc = 1
            continue
        if (line[0] not in skip_char):
            # TODO: replace this with regular expressions!
            r1 = line[7:19] 
            r2 = line[19:31]
            p1 = line[43:55]
            p2 = line[55:67]
            p3 = line[67:79]
            p4 = line[79:91]
            p5 = line[91:103]

            alpha = line[103:112]
            beta = line[112:121]
            gamma = line[121:130]

            # Temperature dependences
            Tmin = line[145:154]
            Tmax = line[155:165]

            index  = line[0:6]
            rtype  = line[131:137]
            formul = line[138:144]
            multiform = line[165:171] # are there duplicate, higher
                                      # version of this reaction?
            # Check if reaction has a comment string
            reac_com = ''
            try:
                if line[180] in skip_char:
                    reac_com = line[181:].replace('\n','')
            except:
                pass
            
            reac = reaction(index, r1, r2, p1, p2, p3, p4, p5, 
                                  alpha, beta, gamma, Tmin, Tmax, 
                                  rtype, formul, multiform, nform=1, 
                                  comment=reac_com)
            
            reac_list.append(reac)
            
            fc = fc+1
    
    f.close()
    print( 'Reading file {} is done!\n'.format(input_file) )
    
    # Add reaction list to network
    network_.add_reaction(reac_list)
    #network_.add_reaction_check(reac_list)  # same as above but slower
    
    # Return the network
    return network_

def read_umist(input_file, comment='UMIST network',
               skip_char=['!','#','\n']):
    '''
    Reads UMIST network to network class object
    '''
    f = open(input_file, 'r')
    linecounter = 0
    
    # initialise network
    network_ = network(comment=comment)
    
    #
    # Convert UMIST reaction type string to KIDA reaction and formula indice
    rstrig_to_rtype = {
        'AD': (4, 3, 'Associative Detachment'),
        'CD': (1, 3, 'Collisional Dissociation'),
        'CE': (5, 3, 'Charge Exchange'),
        'CP': (1, 1, 'Cosmic-Ray Proton (CRP)'),
        'CR': (17, 11, 'Cosmic-Ray Photon (CRPHOT)'),
        'DR': (8, 3, 'Dissociative Recombination'),
        'IN': (4, 3, 'Ion-Neutral'),
        'MN': (4, 3, 'Mutual Neutralisation'),
        'NN': (4, 3, 'Neutral-Neutral'),
        'PH': (3, 2, 'Photoprocess'),
        'RA': (6, 3, 'Radiative Association'),
        'REA': (8, 3, 'Radiative Electron Attachment'),
        'RR': (8, 3, 'Radiative Recombination')
        }
    
    reac_list = []
    
    for line in f:
        if (line[0] not in skip_char):
            tmp = line.split(':')
            index, rstring, r1, r2, p1, p2, p3, p4,   \
            multiform, alpha, beta, gamma, Tmin, Tmax = tmp[0:14]
            p5 = ''
            
            if (r2 in ['CRP','CRPHOT']):
                alpha = np.double(alpha) / 1.3e-17
                
            if (p2.upper() in ['PHOTON']):
                p2 = ''

            rtype = rstrig_to_rtype[rstring][0]
            formul = rstrig_to_rtype[rstring][1]
            nform = multiform

            reac_com = tmp[16]
            
            reac = reaction(index, r1, r2, p1, p2, p3, p4, p5, 
                                  alpha, beta, gamma, Tmin, Tmax, 
                                  rtype, formul, 1, nform=nform, 
                                  comment=reac_com)
        
            #
            # Multiple temperature ranges
            # Only two different ranges are supported (UMIST12 contains max. two)
            if int(multiform) == 2:
                istart = 18
                try:
                    np.double(tmp[istart])
                except:
                    istart = istart+1
                alpha, beta, gamma, Tmin, Tmax = tmp[istart:istart+5]
                reac_com = tmp[istart:istart+7]

                reac = reaction(index, r1, r2, p1, p2, p3, p4, p5, 
                                  alpha, beta, gamma, Tmin, Tmax, 
                                  rtype, formul, multiform, nform=nform,
                                  comment=reac_com)
            
            reac_list.append(reac)
            
    f.close()
    print( 'Reading file {} is done!\n'.format(input_file) )
    
    # Add reaction list to network
    network_.add_reaction(reac_list)
    
    # Return the network
    return network_

def read_semenov2010(input_file, comment='Semenov (2010) benchmark network',
                     skip_char=['!','#','\n'], skiprdes=True):
    '''
    Reads network file to network class
    '''
    f = open(input_file, 'r')
    linecounter = 0
    
    # initialise network
    network_ = network(comment)
    
    # expected line lengths
    spec_len = 110
    reac_len = 120
    
    reac_list = []
    
    for line in f:
        # Skip comment and lines without reactions
        if (line[0] not in skip_char) and (len(line) >= spec_len):
            
            # pre-process notations
            r1 = line[0:8].rstrip()
            r2 = line[8:16].rstrip().replace('DESORT','DESORB')
            p1 = line[24:32].rstrip()
            p2 = line[32:40].rstrip()
            p3 = line[40:48].rstrip()
            p4 = line[48:56].rstrip()
            p5 = ''

            alpha = line[64:73]
            beta = line[73:82]
            gamma = line[82:91]
            # Temperature dependences
            Tmin = -9999
            Tmax = 9999

            index  = linecounter+1
            rtype  = int(line[91:94])
            # Do minimal rtype conversion to comply with KIDA
            if rtype == 2:     # ion-neutral reactions
                rtype = 4
            if r2.strip() == 'CRPHOT': #
                rtype = 2

            formul = 3
            multiform = 1
            
            if skiprdes:
                if (r1[0] == 'g' and r2[0] == 'g' and p1[0] != 'g'):
                    continue
            
            linecounter = linecounter + 1
            
            reac = reaction(index, r1, r2, p1, p2, p3, p4, p5, 
                                  alpha, beta, gamma, Tmin, Tmax, 
                                  rtype, formul, multiform, nform=1)
            reac_list.append(reac)

    f.close()
    print( 'Reading file {} is done!\n'.format(input_file) )
    
    # Add reaction list to network
    network_.add_reaction(reac_list)
    #network_.add_reaction_check(reac_list)  # same as above but slower
    
    # Return the network
    return network_

def get_binding_energy_garrod06(fname=None, dtype=('a12',float,float), 
                                usecols=(0,2), delimiter=None):
    '''
    Read binding energies from column formatted ascii file. The function returns
    a dictionary (keys: species name, values: binding energy in K).
    
    Default is the Garrod (2006) dataset.
    '''
    
    # Default binding energy database
    fname_ = 'GH06_binding.txt'
    
    if fname:
        fname_ = fname
        
    g06_arr = np.genfromtxt(fname_, skip_footer=True, 
                            dtype=dtype ,usecols=usecols)
    
    g06_dic = {x[0].replace('J','g') : x[1] for x in g06_arr}
    
    return g06_dic
