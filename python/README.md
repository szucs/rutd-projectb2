rutd_chem
=========

Collection of useful tools for tabulating photochemical cross sections and 
working with chemical network.

Chemical network is built up from species object via reaction object to network 
objects. This allows flexibility in sorting, writing and editing network (based 
on species or reaction specific criteria).

Features and options:
--------------------

- Tabulate X-ray ionisation cross sections (Verner & Yakovlev, 1995)
- Tabulate and interpolate FUV cross sections (Heays et al. 2017)
- Compute photochemical rate coefficients using the above data and provided radiation spectrum
- Create, convert (between different code formats) and edit chemical networks

Requirements:
------------

- python (2.7 or 3.5+)
- matplotlib
- numpy
- h5py

Acknowledgement:
---------------

Several functions in the xray_data module are written by Mate Adamkovics. The 
original author is always acknowledged in the function docstring.
