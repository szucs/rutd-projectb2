#!/usr/bin/env python

from setuptools import setup

setup(name='rutd_chem',
      version='0.1',
      install_requires=['numpy', 'matplotlib'],
      provides=['rutd_chem'],
      description="Tool for creating chemical network with frequency dependent X-rays and UV radiation.",
      author='Laszlo Szucs',
      author_email='laszlo.szucs@mpe.mpg.de',
      license='GPLv2',
      url='https://github.com/orgs/RU-TD/teams/project-b2',
      packages=['rutd_chem'],
      package_data={},
     )
